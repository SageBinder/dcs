δ Nat = Zero | Succ Nat

pred (R ~ Nat) : R → R = λ x . γ x { Zero → x | Succ p → p }

τ K X _ _ = X

sub (R ~ Nat) : R → Nat → R =
  λ x . ω sub(y) : K R .
          γ y {
            Zero → x
          | Succ y' → pred (sub y')
          }

δ Bool = True | False

lt =
  ω lt(n) : K (Nat → Bool) .
    λ m .
    γ n {
      Zero → γ m {
               Zero → False
             | Succ _ → True
             }
    | Succ n → γ m {
                 Zero → False
               | Succ m → lt n m
               }
    }

div (R ~ Nat) : R → Nat → Nat =
  λ x y .
  (ω div(x) : K Nat .
    γ x {
      Zero → Zero
    | Succ x' →
      γ lt x y {
        True → Zero
      | False → Succ (div (sub x' (pred y)))
      }
    }
  ) x        