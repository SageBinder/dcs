;; -*- lexical-binding: t; -*-

(require 'dcs-customization)

(defun dcs-mode-debug-message (msg &rest args)
  (when dcs-mode-debug
    (apply #'message msg args)))

(provide 'dcs-debug)
