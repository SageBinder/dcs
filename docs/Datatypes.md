# Datatypes

DCS allows programmers to declare inductive datatypes, similarly to many other functional languages and type theories. This file explains DCS's datatype system, particularly its unusual features.

## Declaring datatypes

Datatypes are declared using similar syntax as in, for example, Haskell, except that DCS uses `δ` instead of the keyword `data`.  For example, here is the declaration for the standard datatype of polymorphic lists, from [Stdlib/Data/List.dcs](../Stdlib/Data/List.dcs):

```
δ List A = Nil | Cons A (List A)
```

There is a type parameter `A`, similarly to what one would find in Haskell, and then constructors listed with the types of their arguments. 

Unlike Haskell, DCS does not impose any requirements on the capitalization of constructors or types.  Like Haskell, DCS keeps the namespaces separate for terms and types, and hence it is fine to use the same name for a type constructor as for a term constructor.  An example is `Pair` from [Stdlib/Data/Pair.dcs](../Stdlib/Data/Pair.dcs):

```
δ Pair A B = Pair A B
```

## Positivity

In order for DCS to enforce statically the termination of all functions, only positive datatypes can be accepted.  A datatype `D` is positive if, in the type for any argument to any constructor, `D` is used recursively only in a *positive position*.  A positive position is one that is to the left of an even number of arrows, counting from the very top of the type.  So for example, in the definition of `List` above, `List A` is used recursively in just one place, namely the second argument of `Cons`.  And there, it occurs under an even number of arrows (from the top of the type), namely 0.

On the other hand, the following datatype is not positive, because the type constructor `Bad` occurs to the left of one arrow 

```
δ Bad = Bad (Bad → Bad)
```

This datatype (the example is well known in type theory) is indeed bad from the point of view of termination, since we can write a diverging function using the type `Bad` using only pattern-matching (no explicit recursion).  In Haskell:

```
data Bad = Bad (Bad -> Bad)

bad :: Bad -> Bad
bad (Bad f) = f (Bad f)

loop = bad (Bad bad)

forceTheLoop = case loop of { Bad _ -> 1 }
```

If you evaluate `forceTheLoop` in ghci, ghci will hang.  The trick of this example is to form something like a self-application of `bad`.  In the definition of `loop`, where we write `bad (Bad bad)`, it is almost like writing `bad bad`, except for the presence of the `Bad` constructor.  And self-application is the paradigmatic way to define diverging code in lambda calculus, so we would be right to be worried about that here.

Preventing negative occurrences of datatypes in the types of their constructors blocks this source of nontermination.  So DCS checks for that, and will report an error when it happens.  The file [tests/varianceErrors.dcs](../tests/varianceErrors.dcs) shows the `Bad` datatype; if you load the file in DCS you will see an error.

## Variances

All types in DCS have a list of *variances*, one for each of its (type) arguments.  The possible variances are:

|Variance | Meaning  |
|-------- | -------- | 
  `+`     | the argument is used only positively 
  `-`     | the argument is used only negatively 
  `±`     | the argument is used both positively and negatively
  `∅`     | the argument is not used at all

To view variances in DCS's structured editing mode (see [EmacsMode.md](EmacsMode.md)), put your cursor on a type symbol and hit `i`.

## Signature functors

In the algebraic approach to datatypes, one considers a datatype to be the least fixed-point of what is called its *signature functor*.  The signature functor shows one level of the datatype's structure, but does not specify the form below that level.  For example, for the `List` datatype in DCS

```
δ List A = Nil | Cons A (List A)
```

the signature functor could be written as

```
δ ListF A R = NilF | ConsF A R
```

Notice that where `List` has one type parameter -- namely, the type `A` for the elements of the list -- `ListF` has two parameters, `A` and `R`.  `A` is again the type for the elements of the list, but `R` is the type for whatever value is going to occupy the place of the tail of the list.  We will consider this further shortly.

First, though, it is quite important to know that in DCS, when you write a datatype declaration with `δ`, you are simultaneously defining the datatype and its signature functor.  The signature functor has the same name as the datatype, but takes an extra type argument (the `R` in the `ListF` example).  And the term constructors for the signature functor have the same names and types as for the datatype, except that recursive occurrences of the signature functor in the type arguments to the constructors are replaced with `R`.

So when you write

```
δ List A = Nil | Cons A (List A)
```

you are declaring this type, and simultaneously also

```
δ List A R = Nil | Cons A R
```

With DCS, we are considering the risk of confusion (with the datatype and signature functor sharing the same names) worth the avoidance of several alternatives: generate new names for the signature functor from the datatype definition (we are seeking to avoid introducing names automatically as much as possible); require the user to introduce such names him/herself (annoying and redundant); and just using signature functors with an explicit type construct for taking the least fixed-point (baroque, also would be complicated for mutually recursive types, which are not supported yet but we hope will be).

Returning to the idea of a signature functor, DCS's type system is aware that datatypes are fixed-points of their signature functors.  So DCS will automatically apply type isomorphisms like

```
List A (List A) ≃ List A
```

Here, the first occurrence (from the left) of `List A` is the signature functor.  The other occurrences are the datatype.  So this says (reading the equation right to left) that the `List A` datatype is isomorphic to its one-step unfolding in terms of its signature functor. 

Another way to think of this is that with an inductive type like `List A`, one can have finite lists of unbounded size. If we only had the signature functor, a list like

```
Cons True (Cons False Nil)
```

could be given the following type, for any type `A` you want:

```
List Bool (List Bool (List Bool A))
```

The signature functor represents one level of nested application of the constructors of the datatype, and here we have three levels (one for the first `Cons`, one for the second, and one more for the `Nil`).  So the type has three applications of the signature functor.  By being the least fixed-point of the signature functor, the list datatype embodies the idea that any finite number of applications of the signature functor is allowed.

Even non-recursive types like `Bool` work this way, and so (at least at the time of writing this) sometimes you will see somewhat unusual types like `Bool *` when browsing DCS type information.  This is just an application of the `Bool` signature functor to a type variable `*` (there is no special meaning for `*`; it is just another type variable). 


Having considered DCS's datatypes, it is time to turn to the critical matter of computing over these with algebras.  Please see [Algebras.md](Algebras.md). The much simpler matter of pattern-matching in DCS is covered in [PatternMatching.md](PatternMatching.md).