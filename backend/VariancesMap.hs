module VariancesMap where

import Pos
import Variance
import qualified Data.Map as M

{- map the positions of declared type constants to
   a list of polarities, one for each of their 
   type parameters.

   We need this so we can check variances for TApps

   For functors of datatypes, we do not explicitly
   list the functorial input as a parameter.
-}
type VariancesMap = M.Map Pos [Variance] 
