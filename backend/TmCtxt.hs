module TmCtxt where

import Pos
import Syntax
import Data.Map as M

{- map a position binding a term variable x to (params,ty), representing
  that x has type forall(params).ty -}
type TmCtxt = M.Map Pos ([TyParamTm],Ty) 
