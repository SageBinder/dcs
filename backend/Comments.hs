{-# LANGUAGE TupleSections #-}
module Comments (cullComments, Comment, Comments, cullForward, cullBackward, cullNested, showComment) where

import Util
import Highlighting
import Pos
import Data.List

{- names for character positions in files -}
type StartPos = CharPos
type EndPos = CharPos

data CommentType = OpenRight | OpenLeft | Closed

instance Show CommentType where
  show OpenRight = "open-right"
  show OpenLeft = "open-left"
  show Closed = "closed"

type Comment = (StartPos,EndPos,CommentType)
type Comments = [Comment]

{- assuming we just began a comment with given ending delimiter,
   collect to the end of that comment.

   The StartPos is for the beginning of the starting delimiter.

   We return position of last character in the comment including ending delimiter.
   Also return the blanked comment, and the rest of the string to process
   after the comment.
-}
cullComment
  :: StartPos -> String -> String {- ending delimiter -} -> Int {- to drop for ending delim -} ->
     (EndPos {- where comment ends -}, String {- blanked comment -}, String {- rest of string to process -})
cullComment p str delim ldelim = 
  let (commentBody,rest) =
        (case (breakL (\ rest -> if isPrefixOf delim rest then Just ([],drop ldelim rest) else Nothing) str) of
           Nothing -> (str,"") -- undelimited comment
           Just (commentBody,_,rest) -> (commentBody,rest)) in
  let len = 2 {- for starting delim -} + length commentBody + ldelim in
    (p + len - 1, -- subtract 1 because p is the start of the starting delimiter 
     take len (repeat ' ') ,
     rest)

{- the list of comments and the second string (it is reversed) are accumulator arguments.
   The resulting String is reversed, to make it more efficient for the second pass. -}
cullForwardh :: StartPos -> String -> Comments -> String -> (Comments,String)
cullForwardh p ('-':c:str) cs culled | c == '-' || c == '.' =
  let (end,blanked,rest) = cullComment p str "\n" 0 in
   cullForwardh
     (end+1) {- end is the last character of the comment-}
     rest ((p,end,OpenRight) : cs) (blanked ++ culled)
cullForwardh p (c:str) cs culled =
  cullForwardh (p+1) str cs (c:culled)
cullForwardh p [] cs culled = (cs,culled)
      
cullForward :: String -> (Comments,String)
cullForward str = cullForwardh 1 str [] ""

-- assumes the string is reversed; will reverse it again in the output
cullBackward :: String -> (Comments,String)
cullBackward str =
  let len = length str in
  let (cs,culled) = cullForward str in
    {- reverse and patch up the end points, change CommentType -}
    (map (\ (s,e,_) -> (len - e + 1, len - s + 1,OpenLeft)) cs, culled)

----------------------------------------------------------------------
-- blanking nested comments
----------------------------------------------------------------------

type CulledT = (Comments,
                Maybe EndPos {- if we found a dangling close-comment delimiter -},
                String {- culled string after latest dangling close-comment delimiter -})

{- a couple functions for updating the CulledT state if it
   does not indicate a dangling close-comment delimiter -}
addToCulledIf :: String -> CulledT -> CulledT
addToCulledIf str (cs,Nothing,str') = (cs,Nothing,str++str')
addToCulledIf _ x = x

addToCommentsIf :: Comment -> CulledT -> CulledT
addToCommentsIf c (cs,Nothing,str) = (c:cs,Nothing,str)
addToCommentsIf _ x = x

{- the Int is the current nesting depth; if it is 0, then there is Nothing
   for the Maybe StartPos.  Otherwise, that StartPos is the start of the
   outermost comment we are nested inside.

   We return a list of character ranges for the comments we have blanked
   from the input String to obtain the output String.  The EndPos, if given,
   is for the latest dangling close-comment delimiter we find.
-}
cullNestedh :: StartPos -> String -> Int -> Maybe StartPos -> (Comments,Maybe EndPos,String)

-- start nested comment
cullNestedh p ('{':'-':str) n mstart =
  let mstart' = if n == 0 then Just p else mstart in
    addToCulledIf "  " $ cullNestedh (p+2) str (n + 1) mstart' 

-- dangling close-comment delimiter
cullNestedh p ('-':'}':str) n _ | n == 0 =
  let (cs,me,culled) = cullNestedh (p+2) str 0 Nothing in
    case me of
      Nothing -> (cs,Just (p+1),culled) -- this is the latest dangling delimiter
      _ -> (cs,me,culled) -- there was a later dangling delimiter 

-- end outermost nested comment
cullNestedh p ('-':'}':str) 1 (Just s)   =
  addToCommentsIf (s,p+1,Closed) $ addToCulledIf "  " $ cullNestedh (p+2) str 0 Nothing

-- end inner nested comment
cullNestedh p ('-':'}':str) n mstart   =
  addToCulledIf "  " $ cullNestedh (p+2) str (n-1) mstart

cullNestedh p (c:str) n mstart =
  addToCulledIf [if n > 0 then ' ' else c] $ cullNestedh (p+1) str n mstart 

cullNestedh p [] n Nothing = ([],Nothing,"")

-- dangling start-comment delimiter, but this just means comment to end of file
cullNestedh p [] n (Just s) = ([(s,p-1,OpenRight)],Nothing,"")
  
cullNested :: String -> (Comments,String)
cullNested s =
  let (cs,me,culled) = cullNestedh 1 s 0 Nothing in
    case me of
      Nothing -> (cs,culled)
      Just e -> ([(1,e,OpenLeft)],take e (repeat ' ') ++ culled)
    

----------------------------------------------------------------------
-- printing lists of Comments
----------------------------------------------------------------------

showComment :: Comment -> String
showComment (start,end,t) =
  "(dcs-mode-highlight-comment \"" ++ show t ++ "\"" ++ elispLocalExtent (start, end) ")"

----------------------------------------------------------------------
-- main functions, combining the above
----------------------------------------------------------------------

cullComments :: String -> (Comments,String)
cullComments str =
  let (cs1,step1) = cullForward str in
  let (cs2,step2) = cullBackward step1 in
  let (cs3,step3) = cullNested step2 in
    (cs1 ++ cs2 ++ cs3, step3)
    
    
