{-# LANGUAGE TupleSections #-}

{- names and data structures for built-in constants -}
module Builtins where

import Pos
import PosMap
import Syntax
import SyntaxHelpers
import System.FilePath
import Trie
import Variance
import VariancesMap
import TmCtxt
import qualified Data.Map as M

data BuiltinTyInfo = BuiltinTyInfo { builtinTyVariances :: [Variance] }
data BuiltinTmInfo = BuiltinTmInfo { builtinType :: ([TyParamTm],Ty) }

builtinsFilename :: FilePath
builtinsFilename = "α"

builtinPos :: Int -> Pos
builtinPos n = mkPos builtinsFilename n

builtinName :: Int -> String -> Var
builtinName n v = (v,builtinPos n)

builtinEntry :: (Int, (String, a)) -> (Var,a)
builtinEntry (n, (v, i)) = (builtinName n v, i)

toDeclList :: Int -> [(String,a)] -> [(Var,a)]
toDeclList n ds = builtinEntry <$> (zip [n..] ds)

builtinTyDecls :: [(Var,BuiltinTyInfo)]
builtinTyDecls = 
  toDeclList 1
  [("Gen", BuiltinTyInfo [Positive]) ]

cGen :: Const
cGen = fst (builtinTyDecls !! 0)

builtinTmDecls :: [(Var,BuiltinTmInfo)]
builtinTmDecls =
  let cx = ("X",builtinPos 1)
      cy = ("Y",builtinPos 2)
      tyx = tyConst cx
      tyy = tyConst cy
      arrxy = arrowTy [tyx] tyy
  in
    toDeclList (1 + length builtinTyDecls)
    [ ("fix", BuiltinTmInfo ([TyParam cx, TyParam cy],
                             TApp (cGen, [arrowTy [arrxy] arrxy])))]


builtinPosMap :: [(Var,a)] -> PosMap
builtinPosMap ds = fromList $ ((\ (v,_) -> (varStr v, v)) <$> ds)

builtinCtxt :: Context
builtinCtxt = (builtinPosMap builtinTmDecls, builtinPosMap builtinTyDecls)

builtinTypeVariances :: VariancesMap
builtinTypeVariances =
  M.fromList $ ((\ (v,BuiltinTyInfo vs {- variances -}) -> (varPos v, vs)) <$> builtinTyDecls)

builtinTmCtxt :: TmCtxt
builtinTmCtxt = M.fromList $ ((\ (v,BuiltinTmInfo pty) -> (varPos v, pty)) <$> builtinTmDecls)